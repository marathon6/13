package com.example.task12

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.task12.databinding.RvItemBinding

class RvAdapter: RecyclerView.Adapter<RvAdapter.RvHolder>() {
    val rvElementList = ArrayList<RvElement>()
    class RvHolder(item: View): RecyclerView.ViewHolder(item){
        val binding = RvItemBinding.bind(item)
        fun bind(element: RvElement) = with(binding){

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RvHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.rv_item, parent, false)
        return RvHolder(view)
    }

    override fun onBindViewHolder(holder: RvHolder, position: Int) {
        holder.bind(rvElementList[position])
    }

    override fun getItemCount(): Int {
        return rvElementList.size
    }

    fun addRvElement (element: RvElement){
        rvElementList.add(element)
        notifyDataSetChanged()
    }
}