package com.example.task12

interface HideBottomNav {
    fun hideBottomNav()
    fun showBottomNav()
}