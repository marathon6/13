package com.example.task12

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import java.lang.RuntimeException

class HomeFragment : Fragment() {
    lateinit var hideBottomNav:HideBottomNav
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        hideBottomNav.showBottomNav()

        val rvView = view.findViewById<RecyclerView>(R.id.rvList)
        val rvAdapter = RvAdapter()

        rvView.layoutManager = LinearLayoutManager(context)
        rvView.adapter = rvAdapter
        var element = RvElement("Заголовок 1", "Описание 1")
        rvAdapter.addRvElement(element)

        element = RvElement("Заголовок 2", "Описание 2")
        rvAdapter.addRvElement(element)

        element = RvElement("Заголовок 3", "Описание 3")
        rvAdapter.addRvElement(element)

        element = RvElement("Заголовок 4", "Описание 4")
        rvAdapter.addRvElement(element)

        element = RvElement("Заголовок 5", "Описание 5")
        rvAdapter.addRvElement(element)

        element = RvElement("Заголовок 6", "Описание 6")
        rvAdapter.addRvElement(element)

        element = RvElement("Заголовок 7", "Описание 7")
        rvAdapter.addRvElement(element)

        element = RvElement("Заголовок 8", "Описание 8")
        rvAdapter.addRvElement(element)

        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is HideBottomNav) {
            hideBottomNav = context
        }
        else throw RuntimeException("Не вижу интерфейса HideBottomNav")
    }

    companion object {
        fun newInstance() = HomeFragment()
    }
}