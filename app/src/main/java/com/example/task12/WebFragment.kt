package com.example.task12

import android.content.ContentValues.TAG
import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.OnBackPressedCallback

class WebFragment : Fragment() {
    lateinit var webView: WebView
    lateinit var webViewState: Bundle
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_web, container, false)
        webView = view.findViewById(R.id.wvWeb)

        webViewSetup(webView)

        requireActivity()
            .onBackPressedDispatcher
            .addCallback(this, object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    Log.d(TAG, "Fragment back pressed invoked")

                    if(webView.canGoBack()) webView.goBack()
                    else requireActivity().onBackPressed()
                }
            }
            )

        return view
    }

    private fun webViewSetup(webView: WebView?) {
        webView?.webViewClient = WebViewClient()
        webView?.apply {
            loadUrl("https://www.google.com")
            settings.safeBrowsingEnabled = true
        }
    }

    /*private fun webViewSetup(webView: WebView?) {
        if(webViewState==null) {
            webView?.webViewClient = WebViewClient()
            webView?.apply {
                loadUrl("https://www.google.com")
                settings.safeBrowsingEnabled = true
            }
        }
        else {
            webView?.webViewClient = WebViewClient()
            webView?.restoreState(webViewState)
        }
    }*/

    override fun onPause() {
        super.onPause()
        webViewState = Bundle()
        webView.saveState(webViewState)
    }

    companion object {
        fun newInstance() = WebFragment()
    }
}